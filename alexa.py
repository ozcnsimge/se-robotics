#!/usr/bin/env python
import logging
import test
import operations as op
import os
import rospy
import threading
from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session
from std_msgs.msg import String

app = Flask(__name__)
ask = Ask(app, "/")

# ROS node, publisher, and parameter.
# The node is started in a separate thread to avoid conflicts with Flask.
# The parameter *disable_signals* must be set if node is not initialized
# in the main thread.

threading.Thread(target=lambda: rospy.init_node('alexa_publisher_node', disable_signals=True)).start()
pub = rospy.Publisher('alexa_pub', String, queue_size=1)

logging.getLogger("flask_ask").setLevel(logging.DEBUG)
logger = app.logger

#URL = "url"
ROBOT = "robot_id"

@ask.launch
def start_warehouse():
	"""Launch the warehouse skills

	Returns:
		Message stating start of the warehouse
		Reprompt in the case intent was not understood
	"""

	#api server url
	#session.attributes[URL] = "http://bfd360dc.ngrok.io"

	session.attributes[ROBOT] = 0

	welcome_msg = render_template('startWarehouse')
	reprompt_msg = render_template('reprompt')
	return question(welcome_msg).reprompt(reprompt_msg)

@ask.intent("StartExperimentIntent", 
			convert={'exp_id': int, 'num_shelves': int, 'num_items': int})
def start_experiment(exp_id, num_shelves, num_items):
	"""Given parameters call method starting a new experiment

	Inputs:
		exp_id: New experiment id
		num_shelves: Number of shelves
		num_items: Number of items

	Returns:
		Message stating start of a new experiment
		Reprompt in the case intent was not understood  
	"""

	start_experiment_msg = op.start_experiment(exp_id=exp_id,
												num_shelves=num_shelves,
												num_items=num_items)
	reprompt_msg = render_template('reprompt')
	return question(start_experiment_msg).reprompt(reprompt_msg)

 
@ask.intent("StopExperimentIntent", convert={'exp_id': int})
def stop_experiment(exp_id):
	"""Given parameters call method terminating an experiment
		
	Inputs:
		exp_id: Experiment id

	Returns:
		Message stating termination of the experiment
		Reprompt in the case intent was not understood 
	"""

	stop_experiment_msg = op.stop_experiment(exp_id=exp_id)
	reprompt_msg = render_template('reprompt')
	return question(stop_experiment_msg).reprompt(reprompt_msg)


@ask.intent("MoveRobotOnXIntent", convert={'robot_id': int, 'distance': int})
def move_robot_on_x(robot_id, distance):
	"""Given parameters call method moving a robot on x axis 

	Inputs:
		robot_id: Robot id
		distance: A distance by which is the robot moved 

	Returns:
		Message stating successful movement of the robot
		Reprompt in the case intent was not understood 
	"""
	if robot_id is None: robot_id = session.attributes[ROBOT]
	else: session.attributes[ROBOT] = robot_id

	move_robot_on_x_msg = op.move_robot_on_x(robot_id=robot_id, dist=distance)
	reprompt_msg = render_template('reprompt')
	return question(move_robot_on_x_msg).reprompt(reprompt_msg)


@ask.intent("MoveRobotOnYIntent", convert={'robot_id': int, 'distance': int})
def move_robot_on_y(robot_id, distance):
	"""Given parameters call method moving a robot on y axis

	Inputs:
		robot_id: Robot id
		distance: A distance by which is the robot moved

	Returns:
		Message stating successful movement of the robot
		Reprompt in the case intent was not understood 
	"""
	if robot_id is None: robot_id = session.attributes[ROBOT]
	else: session.attributes[ROBOT] = robot_id

	move_robot_on_y_msg = op.move_robot_on_y(robot_id=robot_id, dist=distance)
	reprompt_msg = render_template('reprompt')
	return question(move_robot_on_y_msg).reprompt(reprompt_msg)


@ask.intent("RotateRobotIntent", convert={'robot_id': int})
def rotate_robot(robot_id, direction):
	"""Given parameters call method rotating a robot

	Inputs:
		robot_id: Robot id
		drection: A direction in which is the robot rotated

	Returns:
		Message stating successful rotation of the robot
		Reprompt in the case intent was not understood 
	"""
	if robot_id is None: robot_id = session.attributes[ROBOT]
	else: session.attributes[ROBOT] = robot_id

	rotate_robot_msg = op.rotate_robot(robot_id=robot_id, direction=direction)
	reprompt_msg = render_template('reprompt')
	return question(rotate_robot_msg).reprompt(reprompt_msg)


@ask.intent("MoveRobotOnXWithShelfIntent", 
			convert={'robot_id': int, 'distance': int})
def move_robot_on_x_with_shelf(robot_id, distance):
	"""Given parameters call method moving a robot on x axis with a shelf
		
	Inputs:	
		robot_id: Robot id
		distace: A distance by which is the robot moved with a shelf

	Returns:
		Message stating successful movement of the robot
		Reprompt in the case intent was not understood 
	"""
	if robot_id is None: robot_id = session.attributes[ROBOT]
	else: session.attributes[ROBOT] = robot_id

	move_robot_on_x_with_shelf_msg = \
			op.move_robot_on_x_with_shelf(robot_id=robot_id, dist=distance)
	reprompt_msg = render_template('reprompt')
	return question(move_robot_on_x_with_shelf_msg).reprompt(reprompt_msg)


@ask.intent("MoveRobotOnYWithShelfIntent", 
			convert={'robot_id': int, 'distance': int})
def move_robot_on_y_with_shelf(robot_id, distance):
	"""Given parameters call method moving a robot on y axis with a shelf
		
	Inputs:
		robot_id: Robot id
		distace: A distance by which is the robot moved with a shelf

	Returns:
		Message stating successful movement of the robot
		Reprompt in the case intent was not understood 
	"""
	if robot_id is None: robot_id = session.attributes[ROBOT]
	else: session.attributes[ROBOT] = robot_id

	move_robot_on_y_with_shelf_msg = \
			op.move_robot_on_y_with_shelf(robot_id=robot_id, dist=distance)
	reprompt_msg = render_template('reprompt')
	return question(move_robot_on_y_with_shelf_msg).reprompt(reprompt_msg)

@ask.intent("MoveRobotIntent", convert={'robot_id': int,
										'pos_x': int,
										'pos_y': int})
def move_robot(robot_id, pos_x, pos_y):
	"""Given parameters call method moving a robot

	Inputs:
		robot_id: Robot id
		pos_x, pos_y: Coordinates of a robot's desired destination

	Returns:
		Message stating successful rotation of the robot
		Reprompt in the case intent was not understood 
	"""
	if robot_id is None: robot_id = session.attributes[ROBOT]
	else: session.attributes[ROBOT] = robot_id

	if pos_x is None or pos_y is None:
		pos_x, pos_y = 0, 0

	move_robot_msg = op.move_robot(robot_id, pos_x, pos_y)
	reprompt_msg = render_template('reprompt')

	return question(move_robot_msg).reprompt(reprompt_msg)

@ask.intent("LiftShelfIntent", convert={'shelf_id': int})
def lift_shelf(shelf_id):
	"""Call method lifting a shelf

	Inputs:
		shelf_id: Shelf to be lifted

	Returns:
		Message stating shelf was lifted 
		Reprompt in the case intent was not understood
	"""
	lift_shelf_msg = op.lift_shelf(shelf_id, is_lifted=1)
	reprompt_msg = render_template('reprompt')

	return question(lift_shelf_msg).reprompt(reprompt_msg)

@ask.intent("StopLiftShelfIntent", convert={'shelf_id': int})
def stop_lift_shelf(shelf_id):
	"""Call method to stop lifting a shelf

	Inputs:
		shelf_id: Shelf id

	Returns:
		Message stating shelf lifting stopped 
		Reprompt in the case intent was not understood
	"""
	stop_lift_shelf_msg = op.lift_shelf(shelf_id, is_lifted=0)
	reprompt_msg = render_template('reprompt')

	return question(stop_lift_shelf_msg).reprompt(reprompt_msg)

@ask.intent("BringShelfIntent", convert={'shelf_id': int,
										'pos_x': int,
										'pos_y': int})
def bring_shelf(shelf_id, pos_x, pos_y):
	"""Given parameters call method bringing a robot

	Inputs:
		shelf_id: Shelf id
		pos_x, pos_y: Coordinates of a shelf's desired destination

	Returns:
		Message stating successful rotation of the robot
		Reprompt in the case intent was not understood 
	"""
	
	if pos_x is None or pos_y is None:
		pos_x, pos_y = 0, 0

	bring_shelf_msg = op.bring_shelf(shelf_id, pos_x, pos_y)
	reprompt_msg = render_template('reprompt')

	return question(bring_shelf_msg).reprompt(reprompt_msg)

@ask.intent("BringItemIntent", convert={'item_count':int,
										'pos_x': int,
										'pos_y': int})
def bring_item(item_name, item_count, pos_x, pos_y):
	"""Given parameters call method getting shelves with an item

	Inputs:
		item_name: Name of an item
		item_count: Number of items
		pos_x, pos_y: Coordinates of a items's desired destination

	Returns:
		Message stating result of searching for an item 
		Reprompt in the case intent was not understood 
	"""
	if pos_x is None or pos_y is None:
		pos_x, pos_y = 0, 0

	if item_count is None: item_count = 1

	session.attributes['item_name'] = item_name
	session.attributes['item_count'] = item_count
	session.attributes['pos_x'] = pos_x
	session.attributes['pos_y'] = pos_y

	# get list of shelves with item
	bring_item_msg = op.get_shelves_with_item(item_name, item_count, pos_x, pos_y)
	reprompt_msg = render_template('reprompt')

	return question(bring_item_msg).reprompt(reprompt_msg)

@ask.intent("AnswerShelfIntent", convert={'shelf_id': int})
def answer_shelf(shelf_id):
	"""Given parameters call method bringing an item

	Inputs:
		shelf_id: Shelf id

	Returns:
		Message stating result of bringing an item 
		Reprompt in the case intent was not understood 
	"""

	# get session attributes values
	item_name = session.attributes['item_name'] 
	item_count = session.attributes['item_count']
	pos_x = session.attributes['pos_x']
	pos_y = session.attributes['pos_y']

	# delete session attributes values
	session.attributes['item_name'] = None
	session.attributes['item_count'] = None
	session.attributes['pos_x'] = None
	session.attributes['pos_y'] = None

	bring_item_msg = op.bring_item(shelf_id, item_name, item_count, pos_x, pos_y)
	reprompt_msg = render_template('reprompt')

	return question(bring_item_msg).reprompt(reprompt_msg)

@ask.intent("CheckItemIntent", convert={'item_count': int})
def check_item(item_name, item_count):
	"""Given parameters call method checking a state of an item

	Inputs:
		item_name: Name of an item
		item_count: Number of items

	Returns:
		Message stating a state of an item 
		Reprompt in the case intent was not understood 
	"""
	if item_count is None: item_count = 1

	check_item_msg = op.check_item(item_name, item_count)
	reprompt_msg = render_template('reprompt')

	return question(check_item_msg).reprompt(reprompt_msg)

@ask.intent("AddItemIntent", convert={'shelf_id': int, 'item_count': str})
def add_item(shelf_id, item_name, item_count):
	"""Given parameters call method adding an item to shelf

	Inputs:
		shelf_id: Shelf id
		item_name: Name of an item
		item_count: Number of items

	Returns:
		Message stating result of adding an item to shelf
		Reprompt in the case intent was not understood 
	"""
	if item_count is None: item_count = 1

	add_item_msg = op.add_item(shelf_id, item_name, item_count)
	reprompt_msg = render_template('reprompt')

	return question(add_item_msg).reprompt(reprompt_msg)

#The session_ended decorator is for the session ended request:
@ask.session_ended
def session_ended():
    return "{}", 200

#Launch and intent requests can both start sessions. Avoid duplicate code with the on_session_started callback:
@ask.on_session_started
def new_session():
    logger.info('new session started')

#todo handle missing shelf, item

# provides a fall back for unmatched utterances 
@ask.intent("AMAZON.FallbackIntent")
def fall_back():
	logger.debug('Not Understood')	
	not_understood_msg = render_template('notUnderstood')
	return question(not_understood_msg)

# stops warehouse
@ask.intent("AMAZON.StopIntent")
def stop():
	logger.debug('Stop')
	stop_msg = render_template('stopWarehouse')
	return statement(stop_msg)

if __name__ == '__main__':
	app.run(debug=True)
