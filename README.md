# Integration of Web Services and Digital Assistants

Cloud services such as [Amazon Web Services](https://aws.amazon.com/de/), [Microsoft Azure](https://azure.microsoft.com/) or [Google Cloud](https://cloud.google.com/) provide a quick and easy way of creating and hosting web services. Many tools in robotics such as the [Neurorobotics Platform](http://neurorobotics.net/) or the middleware [ROS](http://www.ros.org/) natively support distributed setups and can therefore be hosted on cloud infrastructures. In your lab course project, you will use cloud services mainly in two ways:

*  Implement a natrual language-based control interface for the warehouse robotics environment
*  Host single components of the warehouse system on cloud services (e.g. a database with products currently stored in the warehouse, tracking data from the robots for maintenance, collecting mapping data from the robots etc.)

At the beginning your main task will be to think about possible use cases that you would like to work on and to define which components of the system will run locally or in the cloud. Please also think about how you are going to connect these services (especially the digital assistant) to Gazebo and ROS. We will support you in refining your proposal after the kick-off presentations.

# Useful links

*  Project website of the [Neurorobotics Platform](http://neurorobotics.net/)
*  Project website of the [Gazebo simulator](http://gazebosim.org/)
*  Amazon Alexa [developer website](https://developer.amazon.com/alexa)
*  [Flask-Ask SDK](https://github.com/johnwheeler/flask-ask) for Alexa skill development
*  Google Assistant [developer website](https://developers.google.com/actions/)

# How to run?

```
docker build .
docker run -it [image id]
```
# Create the ngrok tunnel

```
docker ps
```
Copy the container id and open a new bash:
```
docker exec -it [container id] bash
```
In the new bash, run the following command:
```
./root/catkin_ws/src/robotik_praktikum/src/ngrok http 5000
```
