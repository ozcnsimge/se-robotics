#!/usr/bin/env python
from tinydb import TinyDB, Query
from tinydb.operations import increment, decrement
import json

db = TinyDB('/root/catkin_ws/src/robotik_praktikum/src/database/db.json')


def new_shelf(shelf_id, x, y, item_dict):
	"""Create a new shelf and isert it to the database

	Inputs:
		shelf_id: Shelf id
		x, y: Coordinates of the shelf
		item_dict: Dictionary of items placed on the shelf
	"""
	new_shelf = create_shelf(shelf_id, x, y, item_dict)
	insert(new_shelf)


def insert(json):
	"""Insert object to the database
	"""
	db.insert(json)


def create_shelf(shelf_id, x, y, item_dict):
	"""Create json with a new shelf and its content
	"""
	new_shelf = { 'shelf_id': shelf_id,
				'coordinates': {
					'x': x,
					'y': y
					},
				'items': 
					item_dict				
				}
	
	return new_shelf


def get_shelf(item_name, item_count):
	"""Get a shelf with a desired item 
	If multiple shelves satisfy query, return one in random

	Inputs:
		item_name: Name of a desired item

	Returns:
		shelf_id: Shelf id
		x, y: Shelf coordinates x, y
	"""
	query = Query()

	# shelf contains at least one desired item
	match = db.get(query.items[item_name] > 0)

	shelf_id, x, y = None, None, None
	if match is not None:
		shelf_id = match['shelf_id']
		x = match['coordinates']['x']
		y = match['coordinates']['y']
	return shelf_id, x, y

def get_shelf_id(item_name, item_count):
	"""Get a shelf with a desired item 
	If multiple shelves satisfy query, return one in random

	Inputs:
		item_name: Name of a desired item
		item_count: Quantity of desired item

	Returns:
		shelf_id: Shelf id
	"""
	query = Query()

	# shelf contains at least one desired item
	match = db.get(query.items[item_name] >= item_count)

	shelf_id = None
	if match is not None: shelf_id = match['shelf_id']

	return shelf_id

def get_multiple_shelf_ids(item_name, item_count):
	"""Get all the shelves with desired item 
	
	Inputs:
		item_name: Name of a desired item
		item_count: Quantity of desired item

	Returns:
		shelf_ids: List of shelves' ids
	"""
	query = Query()

	# list of shelves containing desired items
	match = db.search(query.items[item_name] >= item_count)

	shelf_ids = []

	for i in match:
		shelf_ids.append(i['shelf_id'])

	return shelf_ids

def get_shelf_coordinates(shelf_id):
	"""Get shelf's coordinates

	Inputs:
		shelf_id: Shelf id

	Returns:
		x, y: Shelf coordinates x, y
	"""
	query = Query()

	# shelf contains at least one desired item
	match = db.get(query['shelf_id'] == shelf_id)

	x, y = None, None
	if match is not None:
		x = match['coordinates']['x']
		y = match['coordinates']['y']
	return x, y

def update_shelf_coordinates(shelf_id, x, y):
	"""Update shelf's coordinates

	Inputs:
		shelf_id: Shelf id
		x, y: Shelf coordinates x, y 
	"""
	query = Query()

	coordinates = {'coordinates': {'x': x, 'y': y}}

	db.update(coordinates, query['shelf_id'] == shelf_id)

	msg = "Shelf {} has new coordinates ({}, {})".format(shelf_id, x, y)
	return msg

def add_item_to_shelf(shelf_id, item_name, item_count):
	"""Add a specific item to a specific shelf

	Inputs: 
		shelf_id: Shelf id
		item_name: Item to be added
		item_count: Number of the same items to be added
	"""
	query = Query()
	#query shelf
	match = db.get(query['shelf_id'] == shelf_id)

	if match is None:
		msg = "Shelf {} not found".format(shelf_id)
	
	# query item
	match = db.get((query['shelf_id'] == shelf_id) & (query.items[item_name] >= 0))
	
	# add item to db
	if match is not None:
		db.update(add_item_count(item_name, item_count), query.shelf_id == shelf_id)
	else:
		db.update(add_new_item(item_name, item_count), query.shelf_id == shelf_id)

	msg = "Item {} {} added to shelf {}".format(item_count, item_name, shelf_id)
	return msg

def remove_item_from_shelf(shelf_id, item_name, item_count):
	"""Remove a specific item from a specific shelf

	Inputs: 
		shelf_id: Shelf id
		item_name: Item to be removed
		item_count: Quantity of an item
	"""
	query = Query()	
	db.update(dec_item_count(item_name, item_count), query.shelf_id == shelf_id)


def add_item_count(item_name, item_count):
	"""Add an item count
	"""
	def transform(doc):
		doc['items'][item_name] += int(item_count)
		
	return transform

def add_new_item(item_name, item_count):
	"""Add a new item 
	"""
	def transform(doc):
		doc['items'][item_name] = item_count
		
	return transform

def inc_item_count(item_name, item_count):
	"""Increase an amount of a specific item
	"""
	def transform(doc):
		doc['items'][item_name] += int(item_count)

	return transform

def dec_item_count(item_name, item_count):
	"""Decrease an amount of a specific item
	"""
	def transform(doc):
		doc['items'][item_name] -= int(item_count)

	return transform


# Item dictionaries 
shelf_dict = {
	'iPhone': 1,
	'Samsung': 2
}
shelf_dict2 = {
	'Nokia': 1,
	'Samsung': 2
}

# Add new shelves
#shelf = new_shelf(1, 2, 3, shelf_dict)
#shelf = new_shelf(2, 3, 3, shelf_dict2)

# Return one shelf containing an iphone
#shelf_id, x,y = get_shelf('iphone')

#print(shelf_id, x, y)

# Remove iphone from the shelf
#remove_item_from_shelf(shelf_id, 'iphone')

#print(get_shelf_id('iphone', 1))