from flask import Flask, request
from flask_restplus import Api, Resource, fields

app = Flask(__name__)

api = Api(app=app,
          version="1.0",
          title="Warehouse Env",
          description="Manage shelves and items")

shelfModel = api.model('shelf',
                       {'ID': fields.String(required=True,
                                       description="ID of the shelf",
                                       help="Name cannot be blank.")})
itemModel = api.model('item',
                      {'ID': fields.String(required=True,
                                        description="ID of the shelf",
                                        help="Name cannot be blank.")})

robotModel = api.model('robot',
                       {'ID': fields.String(required=True,
                                        description="ID of the robot",
                                        help="Name cannot be blank.")})

experimentModel = api.model('experiment',
                       {'ID': fields.String(required=True,
                                        description="ID of the experiment",
                                        help="Name cannot be blank.")})


# Add or retrieve a shelf
nsshelf = api.namespace('shelf', description='Manage shelves')
list_of_shelves = {}
@nsshelf.route("/<int:shelfId>")
class shelfClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'shelfId': 'Specify the Id associated with the shelf'})
    def get(self, shelfId):
        try:
            return {
                "status": "shelf retrieved",
                "id": shelfId
            }

        except KeyError as e:
            nsshelf.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")

        except Exception as e:
            nsshelf.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'shelfId': 'Specify the Id associated with the shelf'})
    @api.expect(shelfModel)
    def post(self, shelfId):
        try:
            return {
                "status": "New shelf added",
                "id": shelfId
            }

        except KeyError as e:
            nsshelf.abort(500, e.__doc__, status="Could not save information", statusCode="500")

        except Exception as e:
            nsshelf.abort(400, e.__doc__, status="Could not save information", statusCode="400")


# Add or retrieve an item
nsitem = api.namespace('item', description='Manage items')
list_of_items = {}
@nsitem.route("/<int:itemId>")
class itemClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'itemId': 'Specify the Id associated with the item'})
    def get(self, itemId):
        try:
            return {
                "status": "item retrieved",
                "name": itemId
            }

        except KeyError as e:
            nsitem.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")

        except Exception as e:
            nsitem.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")


    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'itemId': 'Specify the Id associated with the item'})
    @api.expect(itemModel)
    def post(self, itemId):
        try:
            return {
                "status": "New item added",
                "name": itemId
            }

        except KeyError as e:
            nsitem.abort(500, e.__doc__, status="Could not save information", statusCode="500")

        except Exception as e:
            nsitem.abort(400, e.__doc__, status="Could not save information", statusCode="400")


# Retrieve a shelf id for an item
@nsitem.route("/<int:itemId>/shelf_id")
class itemClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'itemId': 'Specify the Id associated with the item'})
    def get(self, itemId):
        try:
            return {
                "status": "Shelf id of the item retrieved",
                "name": itemId,
                "shelf_id": "shelf_id"
            }

        except KeyError as e:
            nsitem.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")

        except Exception as e:
            nsitem.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")


# Retrieve coordinates of a robot
nsrobot = api.namespace('robot', description='Manage robot')
@nsrobot.route("/<int:robotId>")
class robotClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'robotId': 'Specify the Id associated with the robot'})
    def get(self, robotId):
        try:
            return {
                "status": "robot retrieved",
                "name": robotId
            }

        except KeyError as e:
            nsrobot.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")

        except Exception as e:
            nsrobot.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")

    @nsrobot.route("/<int:robotId>/coordinates")
    class robotClass(Resource):

        @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
                 params={'robotId': 'Specify the Id associated with the robot'})
        def get(self, robotId):
            try:
                return {
                    "status": "Coordinates retrieved",
                    "name": robotId,
                    "position X": "coor_X",
                    "position Y": "coor_Y"
                }

            except KeyError as e:
                nsrobot.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")

            except Exception as e:
                nsrobot.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")


# Retrieve coordinates of a shelf
@nsshelf.route("/<int:shelfId>/coordinates")
class shelfClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'shelfId': 'Specify the Id associated with the shelf'})
    def get(self, shelfId):
        try:
            return {
                "status": "shelf coordinates retrieved",
                "name": shelfId,
                "x": "coor_X",
                "y": "coor_Y"
            }

        except KeyError as e:
            nsrobot.abort(500, e.__doc__, status="Could not retrieve information", statusCode="500")

        except Exception as e:
            nsrobot.abort(400, e.__doc__, status="Could not retrieve information", statusCode="400")


# Move a robot
@nsrobot.route("/<int:robotId>/moveto/step_x_<int:step_x>_step_y_<int:step_y>")
class robotClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'robotId': 'Specify the Id associated with the item',
                     'step_x': 'number of steps in the direction x',
                     'step_y': 'number of steps in the direction y'})
    @api.expect(robotModel)
    def post(self, robotId, step_x, step_y):
        try:
            return {
                "status": "Robot moved",
                "name": robotId,
                "step_x": step_x,
                "step_y": step_y
            }

        except KeyError as e:
            nsitem.abort(500, e.__doc__, status="Could not save information", statusCode="500")

        except Exception as e:
            nsitem.abort(400, e.__doc__, status="Could not save information", statusCode="400")


# Rotate a robot 
@nsrobot.route("/<int:robotId>/rotate/direction_<int:direction>")
class robotClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'robotId': 'Specify the Id associated with the item',
                     'direction': 'Specify the direction of the rotation'})
    @api.expect(robotModel)
    def post(self, robotId, direction):
        try:
            return {
                "status": "Robot rotated",
                "name": robotId,
                "direction": direction
            }

        except KeyError as e:
            nsitem.abort(500, e.__doc__, status="Could not save information", statusCode="500")

        except Exception as e:
            nsitem.abort(400, e.__doc__, status="Could not save information", statusCode="400")


# Move a robot with a shelf
@nsrobot.route("/<int:robotId>/moveto_with_shelf/step_x_<int:step_x>_step_y_<int:step_y>")
class robotClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'robotId': 'Specify the Id associated with the item',
                     'step_x': 'number of steps in the direction x',
                     'step_y': 'number of steps in the direction y'})
    @api.expect(robotModel)
    def post(self, robotId, step_x, step_y):
        try:
            return {
                "status": "Robot with shelf moved",
                "name": robotId,
                "step_x": step_x,
                "step_y": step_y
            }

        except KeyError as e:
            nsitem.abort(500, e.__doc__, status="Could not save information", statusCode="500")

        except Exception as e:
            nsitem.abort(400, e.__doc__, status="Could not save information", statusCode="400")


# Initialize an experiment
nsexperiment = api.namespace('experiment', description='Manage experiment')
@nsexperiment.route("/id_<int:experimentId>/start_experiment/shelves_<int:shelves>_items_<int:items>")
class experimentClass(Resource):
    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'shelves': 'Specify the number of shelves associated with the experiment',
                     'items': 'Specify the number of shelves associated with the experiment'})
    @api.expect(experimentModel)
    def post(self, experimentId, shelves, items):
        try:
            return {
                "status": "New experiment initialized",
                "id": "experimentId",
                "shelves": shelves,
                "items": items
            }

        except KeyError as e:
            nsrobot.abort(500, e.__doc__, status="Could not save information", statusCode="500")

        except Exception as e:
            nsrobot.abort(400, e.__doc__, status="Could not save information", statusCode="400")


# Stop an experiment 
@nsexperiment.route("/id_<int:experimentId>/stop_experiment")
class experimentClass(Resource):

    @api.doc(responses={200: 'OK', 400: 'Invalid Argument', 500: 'Mapping Key Error'},
             params={'experimentId': 'Specify the id associated with the experiment'})
    @api.expect(experimentModel)
    def post(self, experimentId):
        try:
            return {
                "status": "Exited from experiment",
                "id": experimentId
            }

        except KeyError as e:
            nsrobot.abort(500, e.__doc__, status="Could not save information", statusCode="500")

        except Exception as e:
            nsrobot.abort(400, e.__doc__, status="Could not save information", statusCode="400")


if __name__ == '__main__':
    app.run(debug=True)