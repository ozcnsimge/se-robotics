#!/usr/bin/env python
import requests


def start_experiment(url, exp_id, num_shelves, num_items):
    """Send http request to api to initialize a new experiment

    Inputs:
        url: API endpoint
        exp_id: Experiment id
        num_shelves: Number of shelves tu initialize
        num_items: Number of items to initialize
    
    Returns:
        msg: Message stating successful experiment initialization    
    """
    # api-endpoint
    URL = "{}/experiment/id_{}/start_experiment/shelves_{}_items_{}" \
                .format(url, exp_id, num_shelves, num_items)

    # sending get request and saving the response as response object
    r = requests.post(url=URL)

    # extracting data in json format
    data = r.json()
    msg = data['status']
    print(msg)
    return msg

def stop_experiment(url, exp_id):
    """Send http request to api to stop an experiment

    Inputs:
        url: API endpoint
        exp_id: Experiment id
    
    Returns:
        msg: Message stating successful experiment termination    
    """
    # api-endpoint
    URL = "{}/experiment/id_{}/stop_experiment".format(url, exp_id)

    # sending get request and saving the response as response object
    r = requests.post(url=URL)

    # extracting data in json format
    data = r.json()
    msg = data['status']
    print(msg)
    return msg

def move_robot_on_x(url, robot_id, dist):
    """Send http request to api to move a robot on x axis

    Inputs:
        url: API endpoint
        robot_id: Robot id
        dist: A distance by which is the robot moved on x axis 
    
    Returns:
        msg: Message stating successful robot movement
    """
    if robot_id is None: robot_id = 0

    URL = "{}/robot/{}/moveto/step_x_{}_step_y_0".format(url, robot_id, dist)
    r = requests.post(url=URL)

    msg = r.json()['status']    
    return msg

def move_robot_on_y(url, robot_id, dist):
    """Send http request to api to move a robot on x axis

    Inputs:
        url: API endpoint
        robot_id: Robot id
        dist: A distance by which is the robot moved on y axis 
    
    Returns:
        msg: Message stating successful robot movement
    """
    if robot_id is None: robot_id = 0

    URL = "{}/robot/{}/moveto/step_x_0_step_y_{}".format(url, robot_id, dist)
    r = requests.post(url=URL)

    msg = r.json()['status']
    return msg

def rotate_robot(url, robot_id, direction):
    """Send http request to api to rotate a robot 

    Inputs:
        url: API endpoint
        robot_id: Robot id
        direction: Direction in which is the robot rotated 
    
    Returns:
        msg: Message stating successful robot rotation
    """
    if robot_id is None: robot_id = 0

    URL = "{}/robot/{}/rotate/direction_{}".format(url, robot_id, direction)
    r = requests.post(url=URL)

    msg = r.json()['status']
    return msg

def move_robot_on_x_with_shelf(url, robot_id, dist):
    """Send http request to api to move a robot on x axis with a shelf

    Inputs:
        url: API endpoint
        robot_id: Robot id
        dist: A distance by which is the robot moved on x axis with a shelf
    
    Returns:
        msg: Message stating successful robot movement
    """
    if robot_id is None: robot_id = 0

    print(dist)
    URL = "{}/robot/{}/moveto_with_shelf/step_x_{}_step_y_0".format(url,
                                                                    robot_id,
                                                                    dist)
    r = requests.post(url=URL)

    data = r.json()
    msg = data['status']
    return msg

def move_robot_on_y_with_shelf(url, robot_id, dist):
    """Send http request to api to move a robot on y axis with a shelf

    Inputs:
        url: API endpoint
        robot_id: Robot id
        dist: A distance by which is the robot moved on y axis with a shelf
    
    Returns:
        msg: Message stating successful robot movement
    """
    if robot_id is None: robot_id = 0
    
    URL = "{}/robot/{}/moveto_with_shelf/step_x_0_step_y_{}".format(url,
                                                                    robot_id,
                                                                    dist)
    r = requests.post(url=URL)

    data = r.json()
    msg = data['status']
    return msg

#TODO
def move_robot(url):
    # api-endpoint
    URL = url + "/robot/4/moveto/step_x_6_step_y_4"

    # sending get request and saving the response as response object
    r = requests.post(url=URL)

    # extracting data in json format
    data = r.json()
    msg = data['status']
    print(msg)
    return msg

# TODO
def bring_item(url):
    # api-endpoint
    URL = url + "/item/3"

    # sending get request and saving the response as response object
    r = requests.get(url=URL)

    # extracting data in json format
    data = r.json()
    msg = data['status']
    print(msg)
    return msg

#TODO
def bring_shelf(url):
    # api-endpoint
    URL = url + "/shelf/3"

    # sending get request and saving the response as response object
    r = requests.get(url=URL)

    # extracting data in json format
    data = r.json()
    msg = data['status']
    print(msg)
    return msg

#TODO
def add_item(url):
    # api-endpoint
    URL = url + "/item/3"

    # sending get request and saving the response as response object
    r = requests.post(url=URL)

    # extracting data in json format
    data = r.json()
    msg = data['status']
    print(msg)
    return msg
