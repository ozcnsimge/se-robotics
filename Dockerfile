FROM ros:melodic

SHELL ["/bin/bash", "-c"]

RUN mkdir -p ~/catkin_ws/src

RUN cd ~/catkin_ws/src && \
    catkin_create_pkg robotik_praktikum std_msgs rospy roscpp

COPY . root/catkin_ws/src/robotik_praktikum/src/

RUN source /opt/ros/melodic/setup.bash && \
    cd ~/catkin_ws/ && \
    catkin_make

RUN apt update && \
    apt install -y python-pip && \
    pip install flask-ask && \
    pip install requests && \
    pip install tinydb && \
    apt install libpython2.7-stdlib

CMD source ~/catkin_ws/devel/setup.bash && \
    cd root/catkin_ws/src/robotik_praktikum/src && \
    roslaunch robotik_praktikum alexa_server.launch




