import logging as log
import db
import alexa

msg = "To be done"

# TODO robot_id
def bring_shelf(shelf_id, pos_x, pos_y):
    """Bring a shelf to a desired position calling moving robot functions

    Inputs:
        shelf_id: Shelf id
        x, y: Desired destination of a shelf
    
    Returns:
        msg: Message stating result of a shelf movement
    """

    # get shelf coordinates
    x, y = db.get_shelf_coordinates(shelf_id)

    if x is None or y is None: 
        msg = "Shelf {} not found".format(shelf_id)
        return msg

    robot_id = 0 # TODO
    
    # robot move to shelf
    move_robot(robot_id, x, y)

    # lift shelf
    lift_shelf(shelf_id, is_lifted=1)

    # move with shelf to a desired position
    move_robot(robot_id, pos_x, pos_y)

    # stop lifting shelf
    lift_shelf(shelf_id, is_lifted=0)

    # update shelf's coordinates in database
    db.update_shelf_coordinates(shelf_id, pos_x, pos_y)

    msg = "Shelf {} brought to position ({}, {})".format(shelf_id, pos_x, pos_y)
    return msg

def lift_shelf(shelf_id, is_lifted):
    """Lift shelf
    """

    # lift shelf here 
    alexa.pub.publish(str(shelf_id) + " " + str(is_lifted))
    if is_lifted == 1:
        msg = "Shelf {} was lifted".format(shelf_id)
    else:
        msg = "Shelf {} is not lifted anymore".format(shelf_id)
    
    log.info(msg)
    return msg

# TODO...

def start_experiment(exp_id, num_shelves, num_items):
    """Initialize a new experiment

    Inputs:
        exp_id: Experiment id
        num_shelves: Number of shelves tu initialize
        num_items: Number of items to initialize
    
    Returns:
        msg: Message stating successful experiment initialization    
    """
    
    alexa.pub.publish(str(exp_id) + " " + str(num_shelves) + " " + str(num_items))
    msg = "Experiment {} started with {} number of shelves and {} number of items.".format(exp_id, num_shelves, num_items)
    return msg

def stop_experiment(exp_id):
    """Stop an experiment

    Inputs:
        exp_id: Experiment id
    
    Returns:
        msg: Message stating successful experiment termination    
    """

    alexa.pub.publish(str(exp_id))
    msg = "Experiment {} is stopped".format(exp_id)

    return msg

def move_robot(robot_id, x, y):
    """Move robot to desired position

    Inputs:
        robot_id: Robot id
        x, y: Desired destination of a robot
    
    Returns:
        msg: Message stating successful robot movement
    """

    alexa.pub.publish(str(robot_id) + " " + str(x) + " " + str(y))
    msg = "Robot {} moved to position {} {}".format(robot_id, x, y)
    log.info(msg)
    return msg

def move_robot_on_x(robot_id, dist):
    """Move a robot on x axis

    Inputs:
        robot_id: Robot id
        dist: A distance by which is the robot moved on x axis 
    
    Returns:
        msg: Message stating successful robot movement
    """

    alexa.pub.publish(str(robot_id) + " " + str(dist))
    msg = "Robot {} moved on x {} points".format(robot_id, dist)
    log.info(msg)

    return msg

def move_robot_on_y(robot_id, dist):
    """Move a robot on x axis

    Inputs:
        robot_id: Robot id
        dist: A distance by which is the robot moved on y axis 
    
    Returns:
        msg: Message stating successful robot movement
    """
    
    alexa.pub.publish(str(robot_id) + " " + str(dist))
    msg = "Robot {} moved on y {} points".format(robot_id, dist)
    log.info(msg)

    return msg

def rotate_robot(robot_id, direction):
    """Rotate a robot 

    Inputs:
        robot_id: Robot id
        direction: Direction in which is the robot rotated 
    
    Returns:
        msg: Message stating successful robot rotation
    """

    alexa.pub.publish(str(robot_id) + " " + str(direction))
    msg = "Robot {} rotated {}".format(robot_id, direction)
    log.info(msg)

    return msg

# TODO call ros
def move_robot_on_x_with_shelf(robot_id, dist):
    """Move a robot on x axis with a shelf

    Inputs:
        robot_id: Robot id
        dist: A distance by which is the robot moved on x axis with a shelf
    
    Returns:
        msg: Message stating successful robot movement
    """
    
    alexa.pub.publish(str(robot_id) + " " + str(dist))
    msg = "Robot {} with shelf moved on x {} points".format(robot_id, dist)
    log.info(msg)

    return msg

def move_robot_on_y_with_shelf(robot_id, dist):
    """Move a robot on y axis with a shelf

    Inputs:
        robot_id: Robot id
        dist: A distance by which is the robot moved on y axis with a shelf
    
    Returns:
        msg: Message stating successful robot movement
    """
    
    alexa.pub.publish(str(robot_id) + " " + str(dist))
    msg = "Robot {} with shelf moved on y {} points".format(robot_id, dist)
    log.info(msg)

    return msg

def get_shelves_with_item(item_name, item_count, pos_x, pos_y):
    """Get IDs of shelfs with a desired item

    Inputs:
        item_name: Name of desired item
        item_count: Quantity of a desired item
    
    Returns:
        msg: Message stating result of item search   
    """
    shelf_ids = db.get_multiple_shelf_ids(item_name, item_count)

    if shelf_ids is None:
        msg = "Item {} not found".format(item_name)
        return msg

    if shelf_ids == 1:
        msg = bring_item(shelf_ids, item_name, item_count, pos_x, pos_y)
        return msg

    msg = "Item {} found in shelves {}. Which shelf do you prefer?".format(item_name, shelf_ids)
    return msg

def bring_item(shelf_id, item_name, item_count, pos_x, pos_y):
    """Bring an item to desired position

    Inputs:
        item_name: Name of desired item
        item_count: Quantity of a desired item
        pos_x, pos_y: Desired destination of an item
    
    Returns:
        msg: Message stating result of bringing an item    
    """

    if shelf_id is None or item_name is None:
        msg = "Shelf not found".format(shelf_id)
        return msg

    # bring shelf
    bring_shelf(shelf_id, pos_x, pos_y)

    # remove item from the shelf
    db.remove_item_from_shelf(shelf_id, item_name, item_count)

    msg = ""

    if item_count > 1:
        msg = "{} pieces of ". format(item_count)

    alexa.pub.publish(str(item_name) + " " + str(item_count) + " " + str(pos_x) + " " + str(pos_y))
    msg += "item {} brought to position ({}, {})".format(item_name, 
                                                        pos_x, 
                                                        pos_y)
    return msg

#TODO bring shelf and move away shelf
def add_item(shelf_id, item_name, item_count):
    """Add an item to desired shelf

    Inputs:
        shelf_id: Shelf id
        item_name: Name of desired item
        item_count: Quantity of a desired item
    
    Returns:
        msg: Message stating result of adding an item    
    """
    # place item on shelf
    alexa.pub.publish(str(shelf_id) + " " + str(item_name) + " " + str(item_count))
    msg = db.add_item_to_shelf(shelf_id, item_name, item_count)
    return msg

def check_item(item_name, item_count):
    """Check existance of an item and its shelf

    Inputs:
        item_name: Name of desired item
        item_count: Quantity of a desired item
    
    Returns:
        msg: Message stating result of checking an item    
    """

    # find shelf
    shelf_ids = db.get_multiple_shelf_ids(item_name, item_count)

    if shelf_ids is None:
        msg = "Item {} not found".format(item_name)
        return msg

    msg = ""

    if item_count > 1: msg = "{} pieces of ".format(item_count)

    msg += "Item {} found on shelves {}".format(item_name, shelf_ids)
    return msg

